let email = document.querySelector('#email')
let password = document.querySelector('#password')
let gender = document.querySelector('#gender')
let radioBtns = document.querySelectorAll("input[type='radio']")
let checkBtns = document.querySelectorAll("input[type='checkbox']")
let passwordError = document.querySelector('#password-error')
let submit = document.querySelector('#submit')
let error = document.querySelector('#error')
let form = document.querySelector('form')

radioBtns.forEach((btn) => {
    btn.checked = false
})

checkBtns.forEach((btn) => {
    btn.checked = false
})

function validateEmail(email) {
    return String(email).trim().toLowerCase().includes('@')
}

function containsUppercase(pass) {
    return pass.toUpperCase() !== pass;
}
function containsLowerCase(pass) {
    return pass.toLowerCase() !== pass;
}
function containsDigit(pass) {
    return /\d/.test(pass);
}

function validatePassword(password) {
    passwordError.innerHTML = ''
    let errors = ``;
    let validPass = true
    if (password.trim().length <= 6) {
        errors += `<li>password should be greater than 6 </li>`
        validPass = false
    }
    if (!containsUppercase(password)) {
        errors += `<li>password should have atleast 1 lowercase </li>`
        validPass = false
    }
    if (!containsLowerCase(password)) {
        errors += `<li>password should have atleast 1 uppercase </li>`
        validPass = false
    }
    if (!containsDigit(password)) {
        errors += `<li>password should have digits (0-9) </li>`
        validPass = false
    }
    passwordError.innerHTML = errors
    return validPass
}


function permissionSelect() {
    let checkedCnt = 0;
    for (const check of checkBtns) {
        // console.log(check);
        // console.log(check.checked);
        if (check.checked) checkedCnt += 1;
    }
    return checkedCnt >= 2;
}


form.addEventListener('submit', (e) => {
    e.preventDefault()
    error.textContent = ''
    if (!validateEmail(email.value)) {
        error.textContent = 'please fill correct email'
        error.classList.add('error')
        return;
    }
    if (!validatePassword(password.value)) {
        return;
    }
    if (gender.value === '') {
        error.textContent = 'please select gender'
        error.classList.add('error')
        return;
    }

    if (!permissionSelect()) {
        error.textContent = 'please select 2 permission'
        error.classList.add('error')
        return;
    }

    // form.style.display = 'none'
    // document.querySelector('.container').style.display = 'none'
    error.textContent = 'submitted'
    error.classList.add('success')
    // const html = `
    // <div>
    //     <p>${email.value}</p>
    //     <p style='margin:10px 0'>${password.value}</p>
    //     <p>${gender.value}</p>
    // </div>
    // `
    // reset all the fields after submitting
    email.value = password.value = gender.value = ''
    for (const check of checkBtns) {
        check.checked = false
    }

    // document.querySelector('.formSubmitted').insertAdjacentHTML('afterbegin', html)


})

